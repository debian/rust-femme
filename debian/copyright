Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: femme
Upstream-Contact: https://github.com/lrlna/femme/issues
Source: https://github.com/lrlna/femme
 .
 Repackaged, excluding non-DFSG files:
  * images with embedded copyright-protected-and-not-licensed ICC profile
Files-Excluded:
 ndjson.png
 pretty.png

Files: *
Copyright:
  2020  Irina Shestak <shestak.irina@gmail.com>
License-Grant:
 MIT OR Apache-2.0
License: Apache-2.0 or Expat
Reference:
 Cargo.toml
 LICENSE-APACHE
 LICENSE-MIT
 README.md

Files:
 debian/*
Copyright:
  2022-2024  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This packaging is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3,
 or (at your option) any later version.
License: GPL-3+
Reference: debian/copyright

License: Apache-2.0
Reference: /usr/share/common-licenses/Apache-2.0

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction,
 including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice
 shall be included in all copies or substantial portions
 of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO
 THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3
